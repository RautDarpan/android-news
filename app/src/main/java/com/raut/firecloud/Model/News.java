package com.raut.firecloud.Model;

import java.io.Serializable;

/**
 * Created by 3543 on 02-10-2016.
 */
public class News implements Serializable {

    private String newsTitle;
    private String newsDetails;
    private String newsDate;
    private String newsPicUrl;

    /*news Model to get and set news data*/
    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsDetails() {
        return newsDetails;
    }

    public void setNewsDetails(String newsDetails) {
        this.newsDetails = newsDetails;
    }

    public String getNewsPicUrl() {
        return newsPicUrl;
    }

    public void setNewsPicUrl(String newsPicUrl) {
        this.newsPicUrl = newsPicUrl;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

}

