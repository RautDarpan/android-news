package com.raut.firecloud.Model;

import java.io.Serializable;

/**
 * Created by 3543 on 11-10-2016.
 */
public class Image implements Serializable {

    private String imageUrl;
    private String imageTitle;

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
