package com.raut.firecloud.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.raut.firecloud.Model.Image;
import com.raut.firecloud.R;
import com.raut.firecloud.Utils.Config;
import com.raut.firecloud.adapter.GalleryAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GalleryActivity extends AppCompatActivity {
    private ArrayList<Image> images;
    private ProgressDialog pDialog;
    private GalleryAdapter mAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Firebase.setAndroidContext(this);
        // push();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        pDialog = new ProgressDialog(this);
        images = new ArrayList<>();


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        //getNews
        Firebase newsPush = new Firebase(Config.FIREBASE_IMAGE_URL);
        newsPush.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> objectMap = (HashMap<String, Object>)
                        dataSnapshot.getValue();
                for (Object obj : objectMap.values()) {
                    if (obj instanceof Map) {
                        Map<String, Object> mapObj = (Map<String, Object>) obj;
                        Image imageModel = new Image();
                        imageModel.setImageUrl((String) mapObj.get("imageUrl"));
                        imageModel.setImageTitle((String) mapObj.get("imageTitle"));
                        images.add(imageModel);
                    }
                }
                mAdapter = new GalleryAdapter(getApplicationContext(), images);
                recyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    public void push() {
        Firebase newsPush = new Firebase(Config.FIREBASE_IMAGE_URL);
        Image imageModel = new Image();
        imageModel.setImageUrl("http://images.clipartpanda.com/bird-clipart-Bird-clip-art-cute-clipart-best.png");
        newsPush.push().setValue(imageModel);
        Toast.makeText(getApplication(), "gallery Successfully Added", Toast.LENGTH_LONG).show();
    }


}
