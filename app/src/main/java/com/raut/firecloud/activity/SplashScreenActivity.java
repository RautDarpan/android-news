package com.raut.firecloud.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.raut.firecloud.R;


public class SplashScreenActivity extends AppCompatActivity {


    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        context = this;
        launchAppropriate();
    }

    /**
     * Method to decide which activity is to be open  based on login status
     */
    private void launchAppropriate() {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setClass(SplashScreenActivity.this, MainActivity.class);
                        finish();
                        startActivity(intent);
                    }
                }, 2000);
    }
}