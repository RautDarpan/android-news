package com.raut.firecloud.service;

import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.ExecutionException;

/**
 * Created by 3543 on 08-10-2016.
 */
public class SaveImageService extends Service {

    Context context;
    private String url;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Bundle b = new Bundle();
        if (b != null) {
            url = intent.getExtras().getString("URL");
        }
        Bitmap theBitmap = null;
        try {
            theBitmap = Glide.with(context).
                    load(url).
                    asBitmap().
                    into(-1, -1).
                    get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        saveToInternalStorage(theBitmap, context, "image_news");
        return null;
    }

    /**
     * Save it on your device
     **/

    public String saveToInternalStorage(Bitmap bitmapImage, Context context, String name) {


        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir

        String name_ = "news"; //Folder name in device android/data/
        File directory = cw.getDir(name_, Context.MODE_PRIVATE);

        // Create imageDir
        File mypath = new File(directory, name);

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("absolutepath ", directory.getAbsolutePath());
        return directory.getAbsolutePath();
    }


}
