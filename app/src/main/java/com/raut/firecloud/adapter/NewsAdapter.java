package com.raut.firecloud.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.raut.firecloud.Model.News;
import com.raut.firecloud.R;
import com.raut.firecloud.service.SaveImageService;
import com.raut.firecloud.ui.NewsDetailActivity;

import java.util.List;

/**
 * Created by 3543 on 03-10-2016.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private List<News> newsList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cardNewsDate, cardNewsTitle;
        public ImageView thumbnail, favImageButtondark, favImageButtonlight, sharebutton;
        private CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            cardNewsTitle = (TextView) view.findViewById(R.id.card_news_title);
            cardNewsDate = (TextView) view.findViewById(R.id.card_news_date);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            cardView = (CardView) view.findViewById(R.id.card_view);
         /*   favImageButtonlight = (ImageView) view.findViewById(R.id.fav_button_light);
            favImageButtondark = (ImageView) view.findViewById(R.id.fav_button_dark);
            sharebutton = (ImageView) view.findViewById(R.id.share_button);*/
        }
    }


    public NewsAdapter(List<News> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final News news = newsList.get(position);
        holder.cardNewsTitle.setText(news.getNewsTitle());
        holder.cardNewsDate.setText(news.getNewsDate());

  /*      holder.favImageButtonlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favImageButtonlight.setVisibility(View.GONE);
                holder.favImageButtondark.setVisibility(View.VISIBLE);
            }
        });

        holder.favImageButtondark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favImageButtonlight.setVisibility(View.VISIBLE);
                holder.favImageButtondark.setVisibility(View.GONE);
            }
        });
        holder.sharebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                shareImage(Uri.parse(news.getNewsPicUrl()));
                shareText(news.getNewsTitle());
                shareText(news.getNewsDetails());
            }
        });
*/
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, NewsDetailActivity.class);

                News newObject = newsList.get(position);
                Bundle b = new Bundle();
                b.putSerializable("news", newObject);
                i.putExtras(b);
                context.startActivity(i);
            }
        });

/** Download the image using Glide **/

        String url = news.getNewsPicUrl();

        Intent n = new Intent(context, SaveImageService.class);
        Bundle b = new Bundle();
        b.putString("URL", news.getNewsPicUrl());
        context.startService(n);

        Glide.with(context)
                .load(url)
                .crossFade()
                .into(holder.thumbnail);
    }

 /*   // Share image
    private void shareImage(Uri imagePath) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image*//*");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        context.startActivity(Intent.createChooser(sharingIntent, "Share Image Using"));
    }*/

    // Share text
    private void shareText(String text) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");// Plain format text

        // You can add subject also
        /*
         * sharingIntent.putExtra( android.content.Intent.EXTRA_SUBJECT,
		 * "Subject Here");
		 */
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using"));
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }
}