package com.raut.firecloud.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.raut.firecloud.Model.News;
import com.raut.firecloud.R;
import com.raut.firecloud.Utils.Config;
import com.raut.firecloud.adapter.NewsAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 3543 on 03-10-2016.
 */
public class DisplayNewsActivity extends AppCompatActivity {
    private static final String TAG = "DisplayAllNew";
    private List<News> newsList;
    private RecyclerView recyclerView;
    private NewsAdapter mAdapter;
    private ProgressBar progressBar;
    private ImageView backdrop;
    private TextView backImageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_news);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

            Firebase.setAndroidContext(this);
//        backdrop = (ImageView)findViewById(R.id.backdrop);
//        backImageTitle = (TextView)findViewById(R.id.back_image_title);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        /*card in horizonntal view*/
//       RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        //getNews
        Firebase newsPush = new Firebase(Config.FIREBASE_URL_NEWS);
        newsPush.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);

                Map<String, Object> objectMap = (HashMap<String, Object>)
                        dataSnapshot.getValue();
                List<News> newsList = new ArrayList<News>();
                for (Object obj : objectMap.values()) {
                    if (obj instanceof Map) {
                        Map<String, Object> mapObj = (Map<String, Object>) obj;
                        News match = new News();
                        match.setNewsDetails((String) mapObj.get("newsDetails"));
                        match.setNewsDate((String) mapObj.get("newsDate"));
                        match.setNewsTitle((String) mapObj.get("newsTitle"));
                        match.setNewsPicUrl((String) mapObj.get("newsPicUrl"));
                        newsList.add(match);
                    }
                }
                mAdapter = new NewsAdapter(newsList, DisplayNewsActivity.this);
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("NEWSDATA", firebaseError.getMessage());
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(mAdapter);
    }

}