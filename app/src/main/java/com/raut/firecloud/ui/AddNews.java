package com.raut.firecloud.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.raut.firecloud.Model.News;
import com.raut.firecloud.R;
import com.raut.firecloud.Utils.Config;

public class AddNews extends AppCompatActivity {

    private EditText newsTitle, newsDeatils, newsDate, newsImageUrl;
    private Button newsSave, newsShowButton;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);


        Firebase.setAndroidContext(this);
//        Layout Edit,Button etc.
        newsSave = (Button) findViewById(R.id.save_news);
        newsShowButton = (Button) findViewById(R.id.show_list_news);
        newsTitle = (EditText) findViewById(R.id.news_title);
        newsDeatils = (EditText) findViewById(R.id.news_details);
        newsDate = (EditText) findViewById(R.id.news_date);
        newsImageUrl = (EditText) findViewById(R.id.news_imag_link);
        newsSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating firebase object
                Firebase newsPush = new Firebase(Config.FIREBASE_URL_NEWS);
                //Getting values to store
                String newsTitleString = newsTitle.getText().toString().trim();
                String newsDetailsString = newsDeatils.getText().toString().trim();
                String newsDateString = newsDate.getText().toString().trim();
                String newsImageUrlString = newsImageUrl.getText().toString().trim();

                final News news = new News();
                news.setNewsTitle(newsTitleString);
                news.setNewsDetails(newsDetailsString);
                news.setNewsDate(newsDateString);
                news.setNewsPicUrl(newsImageUrlString);
                    newsPush.push().setValue(news);
                    Toast.makeText(getApplication(), "News Successfully Added", Toast.LENGTH_LONG).show();


                //getNews
                newsPush.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            //Getting the data from snapshot
                            News newsData = postSnapshot.getValue(News.class);
                            Log.e("NEWSDATA", newsData.getNewsTitle() + newsData.getNewsDetails() + newsData.getNewsDate() + newsData.getNewsPicUrl());
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        Log.e("NEWSDATA", firebaseError.getMessage());
                    }
                });

                newsTitle.setText("");
                newsDeatils.setText("");
                newsDate.setText("");
                newsImageUrl.setText("");

            }
        });
        newsShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddNews.this, DisplayNewsActivity.class);
                startActivity(i);
            }
        });

    }

//
//    private void storeImageToFirebase() {
//        Firebase newsPush = new Firebase(Config.FIREBASE_URL_NEWS);
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
//        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoUri.getPath(), options);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] bytes = baos.toByteArray();
//        String base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
//
//        // we finally have our base64 string version of the image, save it.
//        newsPush.child("pic").setValue(base64Image);
//        System.out.println("Stored image with length: " + bytes.length);
//    }

}
