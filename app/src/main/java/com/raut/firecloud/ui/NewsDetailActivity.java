package com.raut.firecloud.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.raut.firecloud.Model.News;
import com.raut.firecloud.R;

public class NewsDetailActivity extends AppCompatActivity {

    News news = new News();
    TextView nTtile, nDetails,nDate;

    ImageView nThumnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        nTtile = (TextView) findViewById(R.id.ntitle);
        nDetails = (TextView) findViewById(R.id.ndetails);
        nThumnail = (ImageView) findViewById(R.id.thumbnail);
        nDate = (TextView) findViewById(R.id.ndate);
        ImageView ivBack = (ImageView)findViewById(R.id.arrow_back);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            news = (News) b.getSerializable("news");
        }
        nTtile.setText(news.getNewsTitle());
        nDetails.setText(news.getNewsDetails());
        nDate.setText(news.getNewsDate());
        String url = news.getNewsPicUrl();
        Glide.with(this)
                .load(url)
                .crossFade()
                .into(nThumnail);
    }
}
